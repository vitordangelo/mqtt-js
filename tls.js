var mqtt = require('mqtt')
var fs = require('fs')
var KEY = fs.readFileSync('./privkey.pem');
var CERT = fs.readFileSync('./cert.pem');
var CAfile = [fs.readFileSync('./chain.pem')];

var PORT = 8883
var HOST = 'mqttmodule.ml'

var options = {
  port: PORT,
  host: HOST,
  key: KEY,
  cert: CERT,
  ca: CAfile,
  rejectUnauthorized: false,
  protocol: 'mqtts'
}

var client = mqtt.connect(options)

client.subscribe('messages')
client.publish('messages', 'Current time is: ' + new Date())
client.on('message', function (topic, message) {
  console.log(message)
  console.log(topic)
})

client.on('connect', function () {
  console.log('Connected')
})