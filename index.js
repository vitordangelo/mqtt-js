const mqtt = require('mqtt')
// const client = mqtt.connect('mqtt://v2alarm.ddns.net')
const client = mqtt.connect('mqtt://mqttmodule.ml')

client.on('connect', function () {
  client.subscribe('#')
})

client.on('message', function (topic, message, packet) {
  console.log(`Message: ${message} --- Topic: ${topic}`)
  console.log(packet)
  console.log(`Time: ${Date()}`)
  console.log('---------------------------------------------------------------------')
})

client.getLastMessageId()